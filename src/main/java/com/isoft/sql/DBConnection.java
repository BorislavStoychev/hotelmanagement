package com.isoft.sql;

import com.isoft.util.PropertyMaker;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection
{
    private static Connection conn;
    private static PropertyMaker property = new PropertyMaker("src/main/resources/db.properties");

    private static String url = property.getPropertyValue("db.conn.url");
    private static String username = property.getPropertyValue("db.username");
    private static String password = property.getPropertyValue("db.password");

    private static final Logger LOGGER = Logger.getLogger(DBConnection.class);

    private DBConnection()
    {}

    public static Connection connect()
    {
        try
        {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url + "?user=" + username + "&password=" + password);
            return conn;
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        catch (ClassNotFoundException e)
        {
            LOGGER.error(e);
        }
        return null;
    }
}
