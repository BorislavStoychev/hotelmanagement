package com.isoft.service;

import com.isoft.dao.impl.HotelsDaoImpl;
import com.isoft.model.Hotel;

import java.util.List;

/**
 * @author Borislav Stoychev
 */
public class HotelsService
{
    private final HotelsDaoImpl hotelsDao = new HotelsDaoImpl();

    public Hotel getById(Integer id)
    {
       return hotelsDao.getById(id);
    }

    public List<Hotel> getAll()
    {
        return hotelsDao.getAll();
    }

    public void save(Hotel hotelToSave)
    {
        hotelsDao.save(hotelToSave);
    }

    public void update(Hotel hotelToUpdate)
    {
        hotelsDao.update(hotelToUpdate);
    }

    public void delete(Hotel hotelToDelete)
    {
        hotelsDao.delete(hotelToDelete);
    }
}
