package com.isoft.service;

import com.isoft.dao.impl.TouristsDaoImpl;
import com.isoft.model.Tourist;

import java.util.List;

/**
 * @author Borislav Stoychev
 */
public class TouristsService
{
    private final TouristsDaoImpl touristsDao = new TouristsDaoImpl();

    public Tourist getById(Integer id)
    {
        return touristsDao.getById(id);
    }

    public List<Tourist> getAll()
    {
        return touristsDao.getAll();
    }

    public void save(Tourist touristToSave)
    {
        touristsDao.save(touristToSave);
    }

    public void update(Tourist touristToUpdate)
    {
        touristsDao.update(touristToUpdate);
    }

    public void delete(Tourist touristToDelete)
    {
        touristsDao.delete(touristToDelete);
    }
}