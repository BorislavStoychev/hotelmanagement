package com.isoft.dao.impl;

import com.isoft.dao.api.Dao;
import com.isoft.model.Hotel;
import com.isoft.sql.DBConnection;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Borislav Stoychev
 */
public class HotelsDaoImpl implements Dao<Hotel, Integer>
{
    private Connection connection;

    private static final String INSERT_HOTELS_SQL = "INSERT INTO \"Hotels\".hotels(hotel_name, price_per_night, address) VALUES (?, ?, ?);";
    private static final String SELECT_HOTEL_BY_ID = "SELECT hotel_id, hotel_name, price_per_night, address FROM \"Hotels\".hotels WHERE hotel_id = ?";
    private static final String SELECT_ALL_HOTELS = "SELECT * FROM \"Hotels\".hotels";
    private static final String DELETE_HOTEL = "DELETE FROM \"Hotels\".hotels WHERE hotel_id = ?;";
    private static final String UPDATE_HOTEL = "UPDATE \"Hotels\".hotels SET hotel_name = ?, price_per_night = ?, address = ? WHERE hotel_id = ?;";

    private static final Logger LOGGER = Logger.getLogger(HotelsDaoImpl.class);

    @Override
    public Hotel getById(Integer id)
    {
        Hotel hotel = null;
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_HOTEL_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next())
            {
                int hotelId = rs.getInt("hotel_id");
                String hotelName = rs.getString("hotel_name");
                double pricePerNight = rs.getDouble("price_per_night");
                String address = rs.getString("address");
                hotel = new Hotel(hotelId,hotelName,pricePerNight,address);
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
        return hotel;
    }

    @Override
    public List<Hotel> getAll()
    {
        List <Hotel> hotels = new ArrayList<>();
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_HOTELS);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int hotelId = rs.getInt("hotel_id");
                String hotelName = rs.getString("hotel_name");
                double pricePerNight = rs.getDouble("price_per_night");
                String address = rs.getString("address");
                hotels.add(new Hotel(hotelId, hotelName, pricePerNight, address));
            }
        }
        catch (SQLException e)
        {
           LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
        return hotels;
    }

    @Override
    public void save(Hotel hotelToSave)
    {
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_HOTELS_SQL);

            preparedStatement.setString(1, hotelToSave.getHotelName());
            preparedStatement.setDouble(2, hotelToSave.getPricePerNight());
            preparedStatement.setString(3, hotelToSave.getAddress());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
           LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
    }

    @Override
    public void update(Hotel hotelToUpdate)
    {
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_HOTEL);

            preparedStatement.setString(1, hotelToUpdate.getHotelName());
            preparedStatement.setDouble(2, hotelToUpdate.getPricePerNight());
            preparedStatement.setString(3, hotelToUpdate.getAddress());
            preparedStatement.setInt(4,hotelToUpdate.getHotelId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
    }

    @Override
    public void delete(Hotel hotelToDelete)
    {
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_HOTEL);

            preparedStatement.setInt(1,hotelToDelete.getHotelId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
    }
}
