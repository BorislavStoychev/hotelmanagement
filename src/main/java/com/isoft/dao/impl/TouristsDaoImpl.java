package com.isoft.dao.impl;

import com.isoft.dao.api.Dao;
import com.isoft.model.Tourist;
import com.isoft.sql.DBConnection;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Borislav Stoychev
 */
public class TouristsDaoImpl implements Dao<Tourist, Integer>
{
    private Connection connection;

    private static final String INSERT_TOURIST_SQL = "INSERT INTO \"Hotels\".tourists(hotel_id, first_name, last_name, age) VALUES (?, ?, ?, ?);";
    private static final String SELECT_TOURIST_BY_ID = "SELECT tourist_id, hotel_id, first_name, last_name, age FROM \"Hotels\".tourists WHERE tourist_id =?";
    private static final String SELECT_ALL_TOURISTS = "SELECT * FROM \"Hotels\".tourists";
    private static final String DELETE_TOURIST = "DELETE FROM \"Hotels\".tourists WHERE tourist_id = ?;";
    private static final String UPDATE_TOURIST = "UPDATE \"Hotels\".tourists SET hotel_id = ?, first_name = ?, last_name = ?, age = ? WHERE tourist_id = ?;";

    private static final Logger LOGGER = Logger.getLogger(TouristsDaoImpl.class);

    @Override
    public Tourist getById(Integer id)
    {
        Tourist tourist = null;
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TOURIST_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next())
            {
                int touristId = rs.getInt("tourist_id");
                int hotelId = rs.getInt("hotel_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                int age = rs.getInt("age");
                tourist = new Tourist(touristId, hotelId, firstName, lastName, age);
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
        return tourist;
    }

    @Override
    public List<Tourist> getAll()
    {
        List <Tourist> tourists = new ArrayList<>();
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_TOURISTS);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next())
            {
                int touristId = rs.getInt("tourist_id");
                int hotelId = rs.getInt("hotel_id");
                String firstName = rs.getString("first_name");
                String lastName = rs.getString("last_name");
                int age = rs.getInt("age");
                tourists.add(new Tourist(touristId, hotelId, firstName, lastName, age));
            }
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
        return tourists;
    }

    @Override
    public void save(Tourist touristToSave)
    {
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TOURIST_SQL);

            preparedStatement.setInt(1, touristToSave.getHotelId());
            preparedStatement.setString(2, touristToSave.getFirstName());
            preparedStatement.setString(3, touristToSave.getLastName());
            preparedStatement.setInt(4, touristToSave.getAge());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
    }

    @Override
    public void update(Tourist touristToUpdate)
    {
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TOURIST);

            preparedStatement.setInt(1, touristToUpdate.getHotelId());
            preparedStatement.setString(2, touristToUpdate.getFirstName());
            preparedStatement.setString(3, touristToUpdate.getLastName());
            preparedStatement.setInt(4, touristToUpdate.getAge());
            preparedStatement.setInt(5, touristToUpdate.getTouristId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
    }

    @Override
    public void delete(Tourist touristToDelete)
    {
        try
        {
            connection = DBConnection.connect();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TOURIST);

            preparedStatement.setInt(1,touristToDelete.getTouristId());
            preparedStatement.executeUpdate();
        }
        catch (SQLException e)
        {
            LOGGER.error(e);
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (SQLException e)
            {
                LOGGER.error(e);
            }
        }
    }
}
