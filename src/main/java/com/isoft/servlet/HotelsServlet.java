package com.isoft.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.model.Hotel;
import com.isoft.service.HotelsService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Borislav Stoychev
 */
public class HotelsServlet extends HttpServlet
{
    private final HotelsService hotelsService = new HotelsService();
    private ObjectMapper mapper = new ObjectMapper();
    private static final Logger LOGGER = Logger.getLogger(HotelsServlet.class);

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        try
        {
            Hotel hotel = mapper.readValue(request.getReader(), Hotel.class);
            hotelsService.save(hotel);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("application/json");
        PrintWriter out;
        List <Hotel> hotels = hotelsService.getAll();
        try
        {
            String hotelsJson = mapper.writeValueAsString(hotels);
            out = response.getWriter();
            out.println(hotelsJson);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            Hotel hotel = mapper.readValue(request.getReader(), Hotel.class);
            hotelsService.delete(hotel);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            Hotel hotel = mapper.readValue(request.getReader(), Hotel.class);
            hotelsService.update(hotel);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }
}
