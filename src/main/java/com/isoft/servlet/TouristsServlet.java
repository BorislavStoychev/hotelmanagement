package com.isoft.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.model.Tourist;
import com.isoft.service.TouristsService;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * @author Borislav Stoychev
 */
public class TouristsServlet extends HttpServlet
{
    private TouristsService touristsService = new TouristsService();
    private ObjectMapper mapper = new ObjectMapper();
    private static final Logger LOGGER = Logger.getLogger(TouristsServlet.class);

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            Tourist tourist = mapper.readValue(request.getReader(), Tourist.class);
            touristsService.save(tourist);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("application/json");
        PrintWriter out;
        List<Tourist> tourists = touristsService.getAll();
        try
        {
            String touristsJson = mapper.writeValueAsString(tourists);
            out = response.getWriter();
            out.println(touristsJson);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            Tourist tourist = mapper.readValue(request.getReader(), Tourist.class);
            touristsService.delete(tourist);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        try
        {
            Tourist tourist = mapper.readValue(request.getReader(), Tourist.class);
            touristsService.update(tourist);
        }
        catch(IOException e)
        {
            LOGGER.error(e);
        }
    }
}
