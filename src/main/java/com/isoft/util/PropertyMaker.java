package com.isoft.util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertyMaker
{
    private Properties prop = null;
    private final Logger LOGGER = Logger.getLogger(PropertyMaker.class);

    public PropertyMaker(String path)
    {
        Path myPath = Paths.get(path);
        try
        {
            this.prop = new Properties();
            BufferedReader bf = Files.newBufferedReader(myPath, StandardCharsets.UTF_8);
            prop.load(bf);
        }
        catch (FileNotFoundException e)
        {
            LOGGER.error(e);
        }
        catch (IOException e)
        {
            LOGGER.error(e);
        }
    }

    public String getPropertyValue(String key)
    {
        return this.prop.getProperty(key);
    }

}
